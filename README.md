# Run the project
If you don't have prisma install it using `yarn global add prisma`.

To run the project, cd to prisma folder, then enter the command
`prisma deploy`, then `docker-compose up -d` and finally go back to root folder and execute `node src/index.js`.

## Example singup mutation:
```
mutation {
  signup(username: "aliuser", password: "alipass") {
    token
    user {
      loginCount
    }
  }
}
```

## Example login mutation:
```
mutation {
  login(username: "aliuser", password: "alipass") {
    token
    user {
      loginCount
    }
  }
}
```

## Example login subscription:
```
subscription {
  newLogin{
    username
  }
}
```