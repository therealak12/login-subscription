const jwt = require("jsonwebtoken");
const JWT_KEY = "k45khj234d0fh9h24";

function getUserId(context) {
  const Authorization = context.request.get("Authorization");
  if (Authorization) {
    const token = Authorization.replace("Bearer ", "");
    const { userId } = jwt.verify(token, JWT_KEY);
    return userId;
  }

  throw new Error("Not authenticated");
}

module.exports = {
  JWT_KEY,
  getUserId,
};