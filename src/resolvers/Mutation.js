const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { JWT_KEY, getUserId } = require("../utils");

async function signup(parent, args, context, info) {
  const hashedPassword = await bcrypt.hash(args.password, 10);

  const { password, ...user } = await context.prisma.createUser({
    ...args,
    password: hashedPassword,
    loginCount: 0,
  });

  const token = jwt.sign({ userId: user.id }, JWT_KEY);

  return {
    token,
    user,
  };
}

async function login(parent, args, context, info) {
  const { password, ...user } = await context.prisma.user({
    username: args.username,
  });
  if (!user) {
    throw new Error("No such user found");
  }

  const valid = await bcrypt.compare(args.password, password);
  if (!valid) {
    throw new Error("Invalid password");
  }

  const token = jwt.sign({ userId: user.id }, JWT_KEY);

  await context.prisma.updateUser({
    where: {
      id: user.id,
    },
    data: {
      loginCount: user.loginCount + 1,
    },
  });

  return {
    token,
    user,
  };
}

module.exports = {
  signup,
  login,
};