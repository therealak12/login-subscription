function newLoginSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe.user({ mutation_in: ["UPDATED"] }).node();
}

const newLogin = {
  subscribe: newLoginSubscribe,
  resolve: (payload) => payload,
};

module.exports = {
  newLogin,
};