function info(root, args, context, info) {
  return "This simple app can handle user logins, and enables you to subscribe to them.";
}

module.exports = {
  info,
};