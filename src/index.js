const { GraphQLServer } = require("graphql-yoga");
const Mutation = require("./resolvers/Mutation");
const Subscription = require("./resolvers/Subscription");
const Query = require("./resolvers/Query");
const { prisma } = require("../prisma/generated/prisma-client");

const server = new GraphQLServer({
  typeDefs: "src/schema.graphql",
  resolvers: {
    Query,
    Mutation,
    Subscription,
  },
  context: (request) => {
    return {
      ...request,
      prisma,
    };
  },
});

server.start(() => "Now you can login and observe logins!");